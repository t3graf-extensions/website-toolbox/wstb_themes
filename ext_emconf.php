<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'WsTb: Theme Tool',
    'description' => 'Theme tool for website toolbox',
    'category' => 'module',
    'author' => 'T3graf Development-Team',
    'author_email' => 'development@t3graf-media.de',
    'state' => 'alpha',
    'clearCacheOnLoad' => 0,
    'version' => '0.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '12.4',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
