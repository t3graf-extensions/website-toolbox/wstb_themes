<?php

return [
    'theme_tool' => [
        'parent' => 'web',
        'position' => ['bottom'],
        'access' => 'user',
        'workspaces' => 'live',
        'path' => '/module/T3graf/theme_tool',
        'labels' => 'LLL:EXT:wstb_themes/Resources/Private/Language/locallang_theme_tool.xlf',
        'extensionName' => 'WstbThemes',
        'controllerActions' => [
            \T3graf\WstbThemes\Controller\ThemeToolController::class => [
                'info'
            ]
        ],
    ],

];
